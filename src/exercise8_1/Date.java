package exercise8_1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Date implements Comparable<Date>{
	private int day;
	private int month;
	private int year;
	private static List<Date> dateArr = new ArrayList<>();

	public Date() {
		java.time.LocalDate today = java.time.LocalDate.now();
		this.day = today.getDayOfMonth();
		this.month = today.getMonthValue();
		this.year = today.getYear();
	}
	
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public Date(String date) {
		String[] date_parts = date.split("\\.");
		this.day = Integer.parseInt(date_parts[0]);
		this.month = Integer.parseInt(date_parts[1]);
		this.year = Integer.parseInt(date_parts[2]);
	}


	@Override
	public int compareTo(Date date) {
		return (Integer.compare(this.day, date.day));
	}

	public String toString() {
		return this.day + "." + this.month + "." + this.year;
	}
	
	public boolean equals(Date date_to_compare) {
		if (this.day == date_to_compare.day && this.month == date_to_compare.month && this.year == date_to_compare.year) {
			return true;
		} 
		return false;
	}
	
	public static void main(String args[]) {	
		//dateArr.add(new Date());
		dateArr.add(new Date("19.03.1996"));
		dateArr.add(new Date("30.04.2017"));
		dateArr.add(new Date("17.08.2016"));
		dateArr.add(new Date("09.03.2017"));
		
		
		Collections.sort(dateArr);

		for (Date date: dateArr) {
			System.out.println(date.toString());;
		}
		
	}

	public double getDay() {
		return day;
	}
	
	public void setDay(int day) {
		this.day = day;
	}

	public double getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public double getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
}

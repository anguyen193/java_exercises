package homework5;

public class PurchasedMachinePart extends MachinePart {
	private double price;
	private String supplier;
	
	public PurchasedMachinePart(String name, int number_in_stock, double price, String supplier) {
		super(name, number_in_stock);
		this.price = price;
		this.supplier = supplier;
	}

	public double calculate_inventory_value() {
		return this.price * this.getNumber_in_stock();
	}

	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
}

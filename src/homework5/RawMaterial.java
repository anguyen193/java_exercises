package homework5;

public class RawMaterial extends MachinePart {
	private int amount;
	private double unit_price;
	
	public RawMaterial(String name, int number_in_stock, int amount, double unit_price) {
		super(name, number_in_stock);
		this.amount = amount;
		this.unit_price = unit_price;
	}
	
	public double calculate_inventory_value() {
		return this.unit_price * this.amount * this.getNumber_in_stock();
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public double getUnit_price() {
		return unit_price;
	}

	public void setUnit_price(double unit_price) {
		this.unit_price = unit_price;
	}
}

package homework5;

import java.util.ArrayList;

public abstract class MachinePart {
	private String name;
	private int number_in_stock;
	public static ArrayList<MachinePart> machine_parts = new ArrayList<>();
	
	public MachinePart(String name, int number_in_stock) {
		this.name = name;
		this.number_in_stock = number_in_stock;
	}
	
	public String toString() {
		return "Name: " + this.name + ", In stock: " + this.number_in_stock + ", Value: " + this.calculate_inventory_value() + "\n";
	}
	
	public abstract double calculate_inventory_value();
	
	public static void main(String agrs[]) {
		machine_parts.add(new RawMaterial("Processor", 10, 4, 150));
		machine_parts.add(new RawMaterial("RAM", 12, 6, 80));
		machine_parts.add(new RawMaterial("Graphic card", 8, 12, 200));
		machine_parts.add(new PurchasedMachinePart("Processor", 10, 200, "Amazon"));
		machine_parts.add(new PurchasedMachinePart("RAM", 12, 120, "Verkkokauppa"));
		
		for (MachinePart machine_part : machine_parts) {
			System.out.print(machine_part);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber_in_stock() {
		return number_in_stock;
	}

	public void setNumber_in_stock(int number_in_stock) {
		this.number_in_stock = number_in_stock;
	}
}

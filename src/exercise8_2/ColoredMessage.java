package exercise8_2;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.BorderLayout;

public class ColoredMessage extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JTextField textField = new JTextField(10);
	private ColoredMessagePanel messagePanel = new ColoredMessagePanel();
    
    //Constructor
    public ColoredMessage() {
        //Set title
        this.setTitle("Colored Message");
      
        JPanel buttonBar = new JPanel();
        JPanel inputBar = new JPanel();

        //Add buttons
        JButton redBttn = new JButton("Red");
        redBttn.addActionListener(this);
        buttonBar.add(redBttn);

        JButton greenBttn = new JButton("Green");
        greenBttn.addActionListener(this);
        buttonBar.add(greenBttn);

        JButton blueBttn = new JButton("Blue");
        blueBttn.addActionListener(this);
        buttonBar.add(blueBttn);

        //Add input
        JLabel label = new JLabel("Text:");
        inputBar.add(label);
        inputBar.add(textField);
        JButton insertBtn = new JButton(new AbstractAction("Insert") {
			private static final long serialVersionUID = 1L;

			@Override
        	public void actionPerformed(ActionEvent evt) {
        		String text;
        		text = textField.getText();
        		messagePanel.setDisplayedText(text);
        	}
        });
        inputBar.add(insertBtn);

        //Design button bar
        this.add(buttonBar, BorderLayout.SOUTH);

      	//Design input bar
      	this.add(inputBar, BorderLayout.NORTH);
    	this.add(messagePanel, BorderLayout.CENTER);

      	setDefaultCloseOperation(EXIT_ON_CLOSE);
      	setSize(350, 200);
      	setLocationRelativeTo(null);
    }  

    public void actionPerformed(ActionEvent evt) {
    	//Gets the command text on the button which was clicked
    	String command = evt.getActionCommand();

    	if (command.equals("Red"))
    		messagePanel.setTextColor(Color.red);
    	else if (command.equals("Green"))
    		messagePanel.setTextColor(Color.green);
    	else if (command.equals("Blue"))
    		messagePanel.setTextColor(Color.blue);
    } 

    public static void main(String[] args) {
    	ColoredMessage myframe = new ColoredMessage();
    	myframe.setVisible(true);
    }
}

package exercise8_2;

import javax.swing.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics; 

public class ColoredMessagePanel extends JPanel {
	private static final long serialVersionUID = 1L;

    private Color textColor;
    private Font textFont;
    private String displayedText = "Hello world!";

    //Constructor
    public ColoredMessagePanel() {
        setBackground(Color.white);
        textColor = Color.red;
        textFont = new Font("Serif", Font.BOLD, 24);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        System.out.println("hhaha");
        //Sets the drawing color and font of the graphics context
        g.setColor(textColor);
        g.setFont(textFont);
        //Draws text, baseline of the leftmost character is at position (100, 65)
        g.drawString(this.displayedText, 100, 65);
    }

    //Sets a new text color and repaints this component
    public void setTextColor(Color color) {
        textColor = color;
        repaint();
    }

	public String getDisplayedText() {
		return displayedText;
	}

	public void setDisplayedText(String displayedText) {
		this.displayedText = displayedText;
		repaint();
	}
}

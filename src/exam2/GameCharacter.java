package exam2;

import java.awt.Point;
import java.util.ArrayList;

public class GameCharacter {
	private String type;
	private String name;
	private Point location;
	private static ArrayList<GameCharacter> character_list = new ArrayList<>();
	
	public GameCharacter(String type, String name) {
		this.type = type;
		this.name = name;
		Point point1 = new Point();
		this.location = point1;
	}
	
	public GameCharacter(String type, String name, Point location) {
		this.type = type;
		this.name = name;
		this.location = location;
	}
	
	public String to_string() {
		String data = this.type + " " + this.name + " in location(" + this.location.x + "," + this.location.y + ")";
		return data;
	}
	
	public boolean check_location(GameCharacter character_to_check) {
		return this.location.equals(character_to_check.location);
	}
	
	public static void main(String agrs[]) {
		GameCharacter character1 = new GameCharacter("Archer", "Legolas");
		
		Point location2 = new Point(12, 1);
		GameCharacter character2 = new GameCharacter("Sniper", "Adam", location2);
		
		Point location3 = new Point(12, 1);
		GameCharacter character3 = new GameCharacter("Wizard", "Ana", location3);
		
		Point location4 = new Point(2, 14);
		GameCharacter character4 = new GameCharacter("Archer", "Robinhood", location4);
		
		character_list.add(character1);
		character_list.add(character2);
		character_list.add(character3);
		character_list.add(character4);
		
		for (GameCharacter character : character_list) {
			System.out.print(character.to_string());
			System.out.print("\n");
		}
		
		if(character2.check_location(character3)) {
			System.out.print(character2.name + " has the same location as " + character3.name);
		} else {
			System.out.print(character2.name + " does not have the same location as " + character3.name);
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}
	
	
}

package exercise4_2;

import java.util.ArrayList;

public class Bank_account {
	private int account_number;
	private String customer_name;
	private double balance;
	private static double interest_rate;
	private static ArrayList<String> transaction_arr = new ArrayList<>();

	public Bank_account(int account_number, String customer_name) {
		this.account_number = account_number;
		this.customer_name = customer_name;
		this.balance = 0;
		transaction_arr.add("New " + this.account_number + " " + this.customer_name);
	}
	
	static void setup_interest(double interest) {
		Bank_account.interest_rate = interest;
	}
	
	public double deposit(double deposit_amount) {
		this.balance = this.balance + deposit_amount;
		transaction_arr.add(this.account_number + " " + this.customer_name + " withdraw " + deposit_amount);
		return this.balance;
	}
	
	public double withdraw(double withdrawal_amount) {
		this.balance = this.balance - withdrawal_amount;
		transaction_arr.add(this.account_number + " " + this.customer_name + " withdraw " + withdrawal_amount);
		return this.balance;
	}
	
	public double add_interest() {
		this.balance = this.balance + (this.balance * interest_rate);
		
		return this.balance;
	}
	
	public double calculate_interest(double interest_in_percent, int years) {
		this.balance = this.balance * (1 + interest_in_percent * years);
		
		return this.balance;
	}
	
	public String to_string() {
		return "Mr/Ms " + this.customer_name + ", whose account number is " + this.account_number + ", has " + this.balance + " in his/her bank account.";
	}
	
	public static void main(String args[]) {
		Bank_account[] account_arr = new Bank_account[2];
		
		setup_interest(0.06);
		
		account_arr[0] = new Bank_account(123456789, "Anh Nguyen");
		account_arr[1] = new Bank_account(987654321, "Daniel");
		
		System.out.println(account_arr[0].deposit(2000));
		System.out.println(account_arr[0].withdraw(500));
		
		System.out.println(account_arr[0].add_interest());
		
		System.out.println(account_arr[0].to_string());
		
		System.out.println(transaction_arr);
	}
	

	//Getters and Setters
	public int getAccount_number() {
		return account_number;
	}
	public void setAccount_number(int account_number) {
		this.account_number = account_number;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
}

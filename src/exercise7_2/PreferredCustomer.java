package exercise7_2;

public class PreferredCustomer extends Customer {
	private double purchases;
	
	public PreferredCustomer(int customerNumber, String name, String address, double purchases) {
		super(customerNumber, name, address);
		// TODO Auto-generated constructor stub
		this.purchases = purchases;
	}
	
	public String toString() {
		String str = super.toString() + "\nBonus: " + this.calculateBonus();
		return str;
	}

	public double calculateBonus() {
		if (this.purchases >= 500 && this.purchases <= 1000) {
			return 0.02;
		} else if (this.purchases > 1000) {
			return 0.05; 
		}
		return 0;
	}
}

package exercise7_2;

import java.util.ArrayList;

public class Customer {
	private int customerNumber;
	private String name;
	private String address;
	public static ArrayList<Customer> customerList = new ArrayList<>();
	
	public Customer(int customerNumber, String name, String address) {
		this.customerNumber = customerNumber;
		this.name = name;
		this.address = address;
	}
	
	public String toString() {
		String str = "Customer Name: " + this.name + "\nCustomer number: " + this.customerNumber + "\nAddress: " + this.address;
		return str;
	}
	
	public static void main(String agrs[]) {
		Customer obj1 = new Customer(123, "Petri", "Vaasa");
		Customer obj2 = new Customer(234, "Timo", "Helsinki");
		PreferredCustomer obj3 = new PreferredCustomer(345, "Juho", "Sein�joki", 700);
		PreferredCustomer obj4 = new PreferredCustomer(567, "Niina", "Oulu", 1200);
		
		customerList.add(obj1);
		customerList.add(obj2);
		customerList.add(obj3);
		customerList.add(obj4);
		
		for (Customer customer : customerList) {
			if (customer instanceof PreferredCustomer) {
				System.out.print(customer.toString());
				System.out.print("\n");
				System.out.print("---------------\n");
			} else {
				System.out.print(customer.toString());
				System.out.print("\n");
				System.out.print("---------------\n");
			}
		}
	}
	
	public int getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}

package exercise6_2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;

public class Order {
	private int order_number;
	private String customer_name;
	private String order_date;
	public static HashMap<String, Double> prod_info = new HashMap<String, Double>();
	private ArrayList<OrderRow> order_rows = new ArrayList<>();
	public static HashMap<Integer, Order> collection = new HashMap<>();
	
	public Order(int order_number, String customer_name) {
//		this.order_number = (int)(Math.random()*99)+1;
		this.order_number = order_number;
		this.customer_name = customer_name;
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date dateobj = new Date();
		this.order_date = df.format(dateobj);
	}
	
	public void add_order() {
		Scanner sc = new Scanner(System.in);
		//sc.useDelimiter("\\s*,\\s*");

		String prod_name = "";
		int quantity = 0;
		do {
			System.out.print("Insert product name to add order. Insert 'done' to finish shopping and check the total price: ");
			prod_name = sc.nextLine();
			
			if (Order.prod_info.containsKey(prod_name.toLowerCase())) {
				System.out.print("Quantity: ");
				quantity = sc.nextInt();
				sc.nextLine();
				OrderRow obj = new OrderRow(prod_name, quantity, Order.prod_info.get(prod_name));
				order_rows.add(obj);
			} else if (!prod_name.equals("done")) {
				System.out.print("No products named " + prod_name + " found. Please Try again.\n");
			}
		} while (!prod_name.equals("done"));
	}
	
	public double calculate_price() {
		double total_price = 0;
		for (OrderRow order : order_rows) {
			total_price = total_price + (order.price * order.quantity);
		}
		return total_price;
	}
	
	public StringBuilder format_order() {
		StringBuilder obj = new StringBuilder();
		obj.append("Order number: " + this.order_number + ", Customer: " + this.customer_name + ", Order Date: " + this.order_date + "\n");
		obj.append("Order rows: \n");
		for (OrderRow order : order_rows) {
			obj.append("    Product: " + order.prod_name + ", Quantity: " + order.quantity + ", Price: " + order.price + "\n");
		}
		obj.append("Total price: " + this.calculate_price() + "\n");
		
		return obj;
	}
	
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		
		Order.prod_info.put("meat ball", 12.5);
		Order.prod_info.put("minced meat", 22.5);
		Order.prod_info.put("pulled pork", 2.5);
		Order.prod_info.put("salmon", 52.5);
		Order.prod_info.put("broccoli", 17.5);
		Order.prod_info.put("potato", 4.5);
		Order.prod_info.put("shampoo", 18.7);
		Order.prod_info.put("watermelon", 23.2);

//		System.out.print("Enter your name to start the shopping: ");
//		String name = sc.nextLine();
		Order obj1 = new Order(001, "Anh");
//		obj.add_order();
//		System.out.print(obj.format_order());
		OrderRow row1 = new OrderRow("watermelon", 2, 23.2);
		OrderRow row2 = new OrderRow("shampoo", 3, 18.7);
		
		obj1.order_rows.add(row1);
		obj1.order_rows.add(row2);
		
		Order obj2 = new Order(002, "Liam");
//		obj.add_order();
//		System.out.print(obj.format_order());
		OrderRow row3 = new OrderRow("salmon", 2, 52.5);
		OrderRow row4 = new OrderRow("broccoli", 5, 17.5);
		
		obj2.order_rows.add(row3);
		obj2.order_rows.add(row4);
		
		Order.collection.put(obj1.getOrder_number(), obj1);
		Order.collection.put(obj2.getOrder_number(), obj2);
		
		int order_number = 0;
		do {
			System.out.print("Insert order number: ");
			order_number = sc.nextInt();
			
			if (Order.collection.containsKey(order_number)) {
				Order obj = Order.collection.get(order_number);
				System.out.print(obj.format_order());
			} else if (order_number != -1) {
				System.out.print("No order number " + order_number + " found. Pls Try again.\n");
			}
		} while (order_number != -1);
	}

	public int getOrder_number() {
		return order_number;
	}

	public void setOrder_number(int order_number) {
		this.order_number = order_number;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getOrder_date() {
		return order_date;
	}

	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}
}

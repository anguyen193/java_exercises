package exercise3_1;

public class Rectangle {
	public double left_lower_x;
	public double left_lower_y;
	public double right_upper_x;
	public double right_upper_y;
	
	//Constructor
	public Rectangle() {
		this.left_lower_x = 0;
		this.left_lower_y = 0;
		
		this.right_upper_x = 1;
		this.right_upper_y = 1;
	}
	
	//Constructor
	public Rectangle(double right_upper_x, double right_upper_y) {
		this.left_lower_x = 0;
		this.left_lower_y = 0;

		this.right_upper_x = right_upper_x;
		this.right_upper_y = right_upper_y;
	}
	
	//Constructor
	public Rectangle(double left_lower_x, double left_lower_y, double right_upper_x, double right_upper_y) {
		this.left_lower_x = left_lower_x;
		this.left_lower_y = left_lower_y;

		this.right_upper_x = right_upper_x;
		this.right_upper_y = right_upper_y;
	}
	
	public double calculate_width() {
		return this.right_upper_y - this.left_lower_y;
	}
	
	public double calculate_height() {
		return this.right_upper_x - this.left_lower_x;
	}
	
	public double calculate_area() {
		return calculate_width() * calculate_height();
	}
	
	public static void main(String args[]) {
		Rectangle obj = new Rectangle();
		Rectangle obj_1 = new Rectangle(1, 2);
		Rectangle obj_2 = new Rectangle(1, 2, 3, 4);
		
		System.out.println("Width: " + obj.calculate_width());
		System.out.println("Height: " + obj.calculate_height());
		System.out.println("Area: " + obj.calculate_area());
		
		System.out.println("Width: " + obj_1.calculate_width());
		System.out.println("Height: " + obj_1.calculate_height());
		System.out.println("Area: " + obj_1.calculate_area());
		
		System.out.println("Width: " + obj_2.calculate_width());
		System.out.println("Height: " + obj_2.calculate_height());
		System.out.println("Area: " + obj_2.calculate_area());
	}


	public double getLeft_lower_x() {
		return left_lower_x;
	}
	public void setLeft_lower_x(double left_lower_x) {
		this.left_lower_x = left_lower_x;
	}
	public double getLeft_lower_y() {
		return left_lower_y;
	}
	public void setLeft_lower_y(double left_lower_y) {
		this.left_lower_y = left_lower_y;
	}
	public double getRight_upper_x() {
		return right_upper_x;
	}
	public void setRight_upper_x(double right_upper_x) {
		this.right_upper_x = right_upper_x;
	}
	public double getRight_upper_y() {
		return right_upper_y;
	}
	public void setRight_upper_y(double right_upper_y) {
		this.right_upper_y = right_upper_y;
	}
}

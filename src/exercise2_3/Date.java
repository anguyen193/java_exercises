package exercise2_3;

public class Date {
	private int day;
	private int month;
	private int year;

	public String toString() {
		//return day_string + "." + month_string + "." + year_string;
		return this.day + "." + this.month + "." + this.year;
	}
	
	public static void main(String args[]) {
		Date date = new Date();
		date.setDay(16);
		date.setMonth(7);
		date.setYear(2017);
		System.out.println(date.toString());
	}

	public double getDay() {
		return day;
	}
	
	public void setDay(int day) {
		this.day = day;
	}

	public double getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public double getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
}

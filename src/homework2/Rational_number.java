package homework2;

import java.util.Scanner;

public class Rational_number {
	public int numerator;
	public int denominator;
	
	public Rational_number() {
		this.numerator = 0;
		this.denominator = 1;
	}
	
	public Rational_number(int numerator, int denominator) {
		if ( denominator != 0) {
			this.numerator = numerator;
			this.denominator = denominator;
		} else {
			System.out.println("Error: the denominator can not be 0. It will be assigned as 1 automatically.");
			this.numerator = numerator;
			this.denominator = 1;
		}
	}
	
	public String form_rational_number() {
		return this.numerator + "/" + this.denominator;
	}

	public String rational_to_double() {
		return String.format("%.2f", (double)this.numerator / (double)this.denominator);
	}
	
	public Rational_number multiply_rational_numbers(Rational_number multiplier) {
		int numerator = multiplier.numerator * this.numerator;
		int denominator = multiplier.denominator * this.denominator;
		
		Rational_number result = new Rational_number(numerator, denominator);
		return result;
	}
	
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please choose 2 rational numbers");
		
		Rational_number[] rational_number = new Rational_number[2];

		for (int i =0; i < 2; i++) {
			sc.nextLine();  //skip the line break

			System.out.print("Numerator " + (i+1) + ": ");
			int numerator = sc.nextInt();
			System.out.print("Denominator " + (i+1) + ": ");
			int denominator = sc.nextInt();
			
			rational_number[i] = new Rational_number(numerator, denominator);

			
			
		}
		Rational_number result = rational_number[0].multiply_rational_numbers(rational_number[1]);
		
		System.out.println(result.rational_to_double());
		System.out.println(result.form_rational_number());
		
		sc.close();
	}

	public double getNumerator() {
		return numerator;
	}

	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}

	public double getDenominator() {
		return denominator;
	}

	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}
	
	
}

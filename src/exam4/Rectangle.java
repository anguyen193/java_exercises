package exam4;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

/*
 * Describes the characteristics of a rectangle which can be
 * drawn on a graphics context.
 */
public class Rectangle extends JPanel {
	private static final long serialVersionUID = 1L;
	private int x, y;
	private int width, height;
	private Color color;
	
	public Rectangle() {
		this.setBackground(Color.red);
		//random colour
		int r = (int)(Math.random()*256);
		int g = (int)(Math.random()*256);
		int b = (int)(Math.random()*256);
		color = new Color(r, g, b);
	}
	
	/*
	 * Draws this rectangle on the graphics context which is
	 * passed as a parameter.
	 */
    @Override
    public void paintComponent(Graphics g) {
    	super.paintComponent(g);
    	System.out.println(x + " - " + y + " - " + width + " - " + height);
		Color prev = g.getColor();
		g.setColor(color);
		g.fillRect(x, y, width, height);
		g.setColor(prev);
    }

	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}
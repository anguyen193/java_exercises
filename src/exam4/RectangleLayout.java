package exam4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class RectangleLayout extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JTextField textFile = new JTextField(10);
	private Rectangle rectangle = new Rectangle();
	
	public RectangleLayout() {
		//Set title
		this.setTitle("Rectangle");
		
		JPanel inputBar = new JPanel();
		inputBar.setBackground(Color.white);
		//Add input
        JLabel label = new JLabel("File: ");
        inputBar.add(label);
        
        inputBar.add(textFile);
        
        JButton drawBtn = new JButton("Draw");
        drawBtn.addActionListener(this);
        inputBar.add(drawBtn);
        
        //Position the panels
      	this.add(inputBar, BorderLayout.NORTH);
        this.add(rectangle, BorderLayout.CENTER);
 
      	setDefaultCloseOperation(EXIT_ON_CLOSE);
      	setSize(800, 500);
      	setLocationRelativeTo(null);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		String file = "src/exam4/" + textFile.getText().toLowerCase();
		try(BufferedReader input = new BufferedReader(new FileReader(file))) {
	        String line;
	        while((line = input.readLine()) != null ) {
	        	String[] lineParts = line.split(",");
	        	rectangle.setX(Integer.parseInt(lineParts[0]));
	        	rectangle.setY(Integer.parseInt(lineParts[1]));
	        	rectangle.setWidth(Integer.parseInt(lineParts[2]));
	        	rectangle.setHeight(Integer.parseInt(lineParts[3]));
	        	rectangle.repaint();
	        }
	    } 
	    catch (FileNotFoundException ex1) {
	    	System.out.println(ex1);
	    }
	    catch(IOException ex2){
	       //TODO
	    }
	}

}

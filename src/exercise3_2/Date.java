package exercise3_2;

import java.util.Scanner;

public class Date {
	private int day;
	private int month;
	private int year;

	public Date() {
		java.time.LocalDate today = java.time.LocalDate.now();
		this.day = today.getDayOfMonth();
		this.month = today.getMonthValue();
		this.year = today.getYear();
	}
	
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public Date(String date) {
		String[] date_parts = date.split("\\.");
		this.day = Integer.parseInt(date_parts[0]);
		this.month = Integer.parseInt(date_parts[1]);
		this.year = Integer.parseInt(date_parts[2]);
	}

	public String toString() {
		return this.day + "." + this.month + "." + this.year;
	}
	
	public boolean equals(Date date_to_compare) {
		
		if (this.day == date_to_compare.day && this.month == date_to_compare.month && this.year == date_to_compare.year) {
			return true;
		} else {
			return false;
		}
	}
	
	public static void main(String args[]) {
		Date date_1 = new Date();
		Date date_2 = new Date("19.03.1996");
		
		System.out.println(date_1.toString());
		System.out.println(date_2.toString());

		Scanner sc = new Scanner(System.in);
		System.out.println("Please choose choose 1 date in dd.mm.yyyy format");
		System.out.print("Date: ");
		int day = sc.nextInt();

		System.out.print("Month: ");
		int month = sc.nextInt();

		System.out.print("Year: ");
		int year = sc.nextInt();

		Date date_3 = new Date(day, month, year);
		System.out.println(date_3.equals(date_2));
		System.out.println(date_3.toString());
		
		sc.close();
	}

	public double getDay() {
		return day;
	}
	
	public void setDay(int day) {
		this.day = day;
	}

	public double getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public double getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
}

package homework6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Converter extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JTextField inputField = new JTextField(10);
	private JTextField outputField = new JTextField(10);
	private JTextField amountField = new JTextField(10);
	private JLabel result = new JLabel();
	
	
	public Converter() {
		this.setLayout(null); 
		//Set title
        this.setTitle("Currency Converter");
        
        JLabel titleLabel = new JLabel("EUR GBP JPY SEK USD");
        titleLabel.setBounds(85, 15, 150, 20);
        this.add(titleLabel);
        
        JLabel from = new JLabel("From");
        from.setBounds(20, 50, 40, 20);
        this.add(from);
        inputField.setBounds(55, 50, 85, 20);
        this.add(inputField);
        
        JLabel to = new JLabel("To");
        to.setBounds(170, 50, 40, 20);
        this.add(to);
        outputField.setBounds(190, 50, 90, 20);
        this.add(outputField);
        
        JLabel amount = new JLabel("Amount");
        amount.setBounds(20, 85, 50, 20);
        this.add(amount);
        amountField.setBounds(75, 85, 90, 20);
        this.add(amountField);
        
        JButton calculateBtn = new JButton("Calculate");
        calculateBtn.addActionListener(this);
        calculateBtn.setBounds(180, 85, 100, 20);
        this.add(calculateBtn);
        
        result.setBounds(85, 120, 150, 20);
        this.add(result);

        //set size of the frame
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(320, 220);
        setLocationRelativeTo(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String inputCurrency = inputField.getText();
		String outputCurrency = outputField.getText();
		double amount = Double.parseDouble(amountField.getText());
		
		Calculator obj = new Calculator();
		double answer = obj.convert(inputCurrency, outputCurrency, amount);
		
		result.setText(String.format("%.2f " + inputCurrency.toUpperCase() + " -> %.2f " + outputCurrency.toUpperCase(), amount, answer));
	}
	
}

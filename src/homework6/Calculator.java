package homework6;

import java.util.Arrays;

public class Calculator {
	public static String[] names = {"EUR", "GBP", "JPY", "SEK", "USD"};
	public static double[] rates = {1, 0.8553, 119.773, 9.459, 1.061};
	
	public double convert(String inputCurrency, String outputCurrency, double amount) {
		int indexInput = Arrays.asList(names).indexOf(inputCurrency.toUpperCase());
		int indexOutput = Arrays.asList(names).indexOf(outputCurrency.toUpperCase());
		
		double rate = rates[indexOutput] / rates[indexInput];
		
		return amount * rate;
	}
}

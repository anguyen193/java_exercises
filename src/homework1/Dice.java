package homework1;

public class Dice {
	public int result_1;
	public int result_2;

	public int roll() {
		return (int)(Math.random()*6)+1;
	}
	
	public static void main(String args[]) {
		Dice dice_1 = new Dice();
		Dice dice_2 = new Dice();
		
		do {
			dice_1.setResult_1(dice_1.roll());
			dice_2.setResult_2(dice_2.roll());
			
			System.out.println("Dice 1: " + dice_1.getResult_1() + "; Dice 2: " + dice_2.getResult_2());

		} while (dice_1.getResult_1() != dice_2.getResult_2());
	}

	public int getResult_1() {
		return result_1;
	}

	public void setResult_1(int result_1) {
		this.result_1 = result_1;
	}

	public int getResult_2() {
		return result_2;
	}

	public void setResult_2(int result_2) {
		this.result_2 = result_2;
	}
}

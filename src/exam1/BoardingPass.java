package exam1;

import java.util.Scanner;

public class BoardingPass {
	private String flight_no;
	private int gate;
	private String seat;
	
	public int get_group(String ticket) {
		int group;
		String number_part = ticket.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)")[0];
		int to_number = Integer.parseInt(number_part);
		
		if (to_number >= 1 && to_number <= 8) {
			group = 1;
		} else if (to_number >= 9 & to_number <= 21) {
			group = 2;
		} else {
			group = 3;
		}
		return group;
	}
	
	public static void main(String agrs[]) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Insert flight number: ");
		String flight_no = sc.nextLine();
		
		System.out.print("Insert gate: ");
		int gate = sc.nextInt();
		sc.nextLine();

		System.out.print("Insert seat: ");
		String seat = sc.nextLine();
		
		BoardingPass passenger = new BoardingPass();
		passenger.setFlight_no(flight_no);
		passenger.setGate(gate);
		passenger.setSeat(seat);
		
		System.out.print("Your ticket: \n");
		System.out.print("Flight number: " + passenger.getFlight_no() + "\n");
		System.out.print("Gate: " + passenger.getGate() + "\n");
		System.out.print("Seat: " + passenger.getSeat() + "\n");
		System.out.print("Group: " + passenger.get_group(seat) + "\n");
	}
	
	//Setters and getters
	public String getFlight_no() {
		return flight_no;
	}
	public void setFlight_no(String flight_no) {
		this.flight_no = flight_no;
	}
	public int getGate() {
		return gate;
	}
	public void setGate(int gate) {
		this.gate = gate;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
}

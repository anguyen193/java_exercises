package exercise8_4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class DisplayDistribution extends JFrame {

	private JPanel topPanel;
	private JTextField txtGrade = new JTextField();
	private JButton btnInsert;
	private DistributionPanel distributionPanel = new DistributionPanel();
	private Distribution dis = new Distribution(0, 5);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DisplayDistribution frame = new DisplayDistribution();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DisplayDistribution() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 401);
		topPanel = new JPanel();
		topPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(topPanel, BorderLayout.NORTH);

		JLabel lblGrade = new JLabel("Grade : ");
		lblGrade.setFont(new Font("Tahoma", Font.PLAIN, 12));
		topPanel.add(lblGrade);

		txtGrade.setColumns(10);
		topPanel.add(txtGrade);

		btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (Integer.parseInt(txtGrade.getText()) >=0 && Integer.parseInt(txtGrade.getText()) <=5) {
						distributionPanel.insert(Integer.parseInt(txtGrade.getText()));
						
					} else {
						JOptionPane.showConfirmDialog(null, "Please enter a valid number in range [0 - 5] !", "Confirmation", JOptionPane.CLOSED_OPTION);
					}

				} catch (Exception ne) {
					ne.printStackTrace();
					JOptionPane.showConfirmDialog(null, "Please enter a valid number in range [0 - 5] !", "Confirmation", JOptionPane.CLOSED_OPTION);
				}
				
			}
		});
		topPanel.add(btnInsert);

		getContentPane().add(distributionPanel, BorderLayout.CENTER);
	}

}

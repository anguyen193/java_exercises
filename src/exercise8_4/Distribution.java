package exercise8_4;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Distribution {

	private int min;
	private int max;
	private int[] fre;
	private int count = 0;

	public Distribution(int min, int max) {
		this.min = min;
		this.max = max;
		this.fre = new int[max - min + 1];
		for (int i = 0; i < fre.length; i++) {
			fre[i] = 0;
		}
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}

	public void insert(int newVal) {
		if (newVal >= min && newVal <= max) {
			fre[newVal - min]++;
			count++;
		}
	}

	public BigDecimal getAverage() {
		int sum = 0;
		int minValue = this.min;
		for (int i = 0; i < fre.length; i++) {
			sum += minValue * fre[i];
			minValue++;
		}
		// System.out.println(sum);
		MathContext mc = new MathContext(2, RoundingMode.HALF_UP);
		if (this.count != 0) {
			BigDecimal average = new BigDecimal(sum).divide(new BigDecimal(
					this.count), mc);
			return average;
		} else {
			return new BigDecimal(0);
		}

	}

	public int getFrequency(int val) {
		return fre[val - min];

	}

	public int getCount() {
		return count;
	}
}

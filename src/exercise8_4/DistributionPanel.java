package exercise8_4;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class DistributionPanel extends JPanel {

	private Distribution dis;

	/**
	 * Create the panel.
	 */
	public DistributionPanel() {
		setBackground(Color.white);
		this.dis = new Distribution(0, 5);
	}

	public void insert(int grade) {
		this.dis.insert(grade);
		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.black);
		g.setFont(new Font("Serif", Font.PLAIN, 18));
		g.drawLine(50, 50, 400, 50);
		g.drawLine(50, 150, 400, 150);
		g.drawLine(50, 250, 400, 250);

		g.drawString("10", 20, 50);
		g.drawString("5", 20, 150);
		g.drawString("count= " + this.dis.getCount(), 70, 45);
		g.drawString("avg= " + this.dis.getAverage(), 200, 45);

		for (int i = 0; i <= (this.dis.getMax() - this.dis.getMin()); i++) {
			int fre = this.dis.getFrequency(i);
			int width = 40;
			int height = 20 * fre;
			int startX = 55 * (i + 1);
			int startY = 250 - height;
			
			g.setColor(Color.CYAN);
			g.fillRect(startX, startY, width, height);
			
			g.setColor(Color.black);
			g.drawString(String.valueOf(i), (55*(i+1)) + 20, 280);
			g.drawString(String.valueOf(fre), (55*(i+1)) + 20, 240);
			
			
		}
	}

}

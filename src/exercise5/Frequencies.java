package exercise5;

import java.util.Scanner;

/*a) Write a class that represents a (statistical) distribution so that you can use an object of this class to
calculate the frequencies of integer values min, min+1, min+2, ..., max and the average. Implement
following methods:
 a constructor that takes integer values min and max as parameters and creates an array to store
the frequencies
 a method that inserts a new value to the distribution
 a method that returns the frequency of a specified value
 a method that returns the average of the distribution
 a method that returns how many values are inserted to the distribution
b) Write a class for printing a distribution. It has a reference to a distribution object as its instance variable
and the distribution to be printed is passed as an argument to the constructor. It has a method which prints
the distribution in a neat form.
c) Write a program that calculates the grade distribution of a course. The possible grades are values 0, 1, ...,
5. The program asks the grades form the user. Use a distribution object to do the calculations, and a
printing object to do the printing.
d) Write a program that rolls a dice as many times as the user wants. The program calculates the
distribution of the scores. Use a dice class from Homework 1, use a distribution object for calculations and
use printing object to print the distribution.
*/

public class Frequencies {
	private int min;
	private int max;
	private int count;
	private int[] freq;
	
	public Frequencies(int min, int max) {
		this.min = min;
		this.max = max;
		this.count = 0;
		freq = new int[max - min + 1];
	}
	
	public void insert_value(int value) {
		if (value >= min && value <= max) {
			freq[value - min]++;
		}
	}
	
	public int frequency(int value) {
		if (value >= min && value <= max) {
			return freq[value - min];
		} else {
			return 0;
		}
	}
	
	public double get_average() {
		int sum = 0;
		for (int i = 0; i < freq.length; i++) {
			sum = sum + (this.min + i) * freq[i];
		}
		double average = (double)sum / this.count;
		return average;
	}
	
	public void count(int value) {
		if (value >= min && value <= max) {
			this.count++;
		}
	}
	
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		Frequencies obj = new Frequencies(0, 5);
		
		System.out.print("Insert value: ");
		int value = sc.nextInt();
		obj.insert_value(value);
		obj.count(value);
		
		do {
			System.out.print("Insert value: ");
			value = sc.nextInt();
			
			obj.insert_value(value);
			obj.count(value);
		} while (value != -1);
		
		System.out.println("Frequency");
		for (int i = obj.getMin(); i <= obj.getMax(); i++) {
			System.out.print(String.valueOf(i) + " : " + String.valueOf(obj.frequency(i)) + "\n");
		}
		System.out.print("Total values inserted: " + obj.count + "\n");		
		System.out.print("Average frequency: " + obj.get_average() + "\n");
		System.out.println(obj.freq.length);
		System.out.print(obj.frequency(1));
		
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int[] getFreq() {
		return freq;
	}

	public void setFreq(int[] freq) {
		this.freq = freq;
	}
}

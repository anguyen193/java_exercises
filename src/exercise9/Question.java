package exercise9;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Question {
	@SuppressWarnings("rawtypes")
	public ArrayList<HashMap> questionnaire = new ArrayList<>();
	
	public Question() {
		String filename = "src/exercise9/questionnaire.txt";
		try(BufferedReader input = new BufferedReader(new FileReader(filename))) {
	        String line;
	        int count = 1;
	        String question = "";
	        String rightAnswer = "";
	        HashMap<String, String> aPair = new HashMap<>();
	        while((line = input.readLine()) != null ) {
	        	switch(count) {
	        		case 1: question = line + "\n"; 
	        		break;
	        		
	        		case 2: question += line + "\n";
	        		break;
	        		
	        		case 3: question += line + "\n";
	        		break;
	        		
	        		case 4: question += line + "\n";
	        		break;
	        		
	        		case 6: rightAnswer = line;
	        	}
	        	if (count < 6) {
	        		count++;
	        	} else {
	        		aPair.put("question", question);
	        		aPair.put("answer", rightAnswer);
	        		questionnaire.add(aPair);

	        		//Reset variables
	        		count = 0;
	        		question = "";
	    	        rightAnswer = "";
	    	        aPair = new HashMap<String, String>();
	        	}
	        }
	    } 
	    catch (FileNotFoundException ex1) {
	    	System.out.println(ex1);
	    }
	    catch(IOException ex2){
	       //TODO
	    }
	}
	
	@SuppressWarnings("unchecked")
	public boolean checkAnswer(int nthQuestion, String answer) {
		answer = answer.toUpperCase();
		for (HashMap<String, String> singleQuestion : this.questionnaire) {
	    	if ((nthQuestion) == this.questionnaire.indexOf(singleQuestion)) {
	    		System.out.println(singleQuestion.get("answer").equals(answer));
	    		if (singleQuestion.get("answer").equals(answer)) {
	    			System.out.println(1);
	    			return true;
	    		}
	    		return false;
	    	}
	    }
		return false;
	}
}

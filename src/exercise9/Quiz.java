package exercise9;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Quiz extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JTextArea questionField = new JTextArea(10, 50);; 
	private JTextField answerField = new JTextField(10);
	private int rightAnswers = 0;
	public Question obj = new Question();
	private int size = obj.questionnaire.size();
	@SuppressWarnings("rawtypes")
	private HashMap currentPair = obj.questionnaire.get(0);
	private String currentQuestion = (String) currentPair.get("question");
	private JLabel resultLabel = new JLabel("Right answers/Questions: " + this.rightAnswers + "/" + this.size);

	public Quiz() {
		this.setTitle("GUI Demo");
        this.setLayout(null);

        //add components to the frame
        JLabel questionLabel = new JLabel("Question");
        questionLabel.setBounds(30, 20, 80, 20);
        this.add(questionLabel);
        questionField.setBounds(120, 20, 350, 150);
        this.add(questionField);
        
        JLabel answerLabel = new JLabel("Your answer");
        answerLabel.setBounds(30, 190, 80, 20);
        this.add(answerLabel);
        answerField.setBounds(120, 190, 220, 25);
        this.add(answerField);
        
        JButton checkAnswer = new JButton("Check answer");
        checkAnswer.addActionListener(this);
        checkAnswer.setBounds(350, 190, 120, 25);
        this.add(checkAnswer);

	    questionField.append(this.currentQuestion);
	    
        resultLabel.setBounds(120, 220, 200, 30);
        this.add(resultLabel);
        
        //set size of the frame
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(520, 320);
        setLocationRelativeTo(null);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String answer = answerField.getText();
		int currentIndex = obj.questionnaire.indexOf(currentPair);
		
		
			if (obj.checkAnswer(currentIndex, answer)) {
				System.out.println(1);
				this.rightAnswers++;
				resultLabel.setText("Right answers/Questions: " + this.rightAnswers + "/" + this.size);
			}
			if (currentIndex < this.size-1) {
				currentIndex++;
				this.currentPair = obj.questionnaire.get(currentIndex);
				this.currentQuestion = (String) currentPair.get("question");
				answerField.setText(null);
				questionField.setText(null);
				questionField.setText(this.currentQuestion);
			} else {
				questionField.setText(null);
				questionField.setText("You have successfully completed your questionnaire!");
			}
	}	 
}

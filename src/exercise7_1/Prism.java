package exercise7_1;


public abstract class Prism {
	public double height;
	public double width;
	public double depth;
	
	public Prism(double height, double width, double depth) {
		this.height = height;
		this.depth = depth;
		this.width = width;
	}
	
	public abstract double calculate_volume();
	
	public abstract double calculate_mass();
	
	public static void main(String agrs[]) {
		Mass obj1 = new Mass(10, 15, 8, "Iron", 7870);
		Mass obj2 = new Mass(12, 18, 14, "Cooper", 8940);
		
		System.out.print("Volume: " + obj1.calculate_volume() + " m3\n");
		System.out.print("Mass: " + obj1.calculate_mass() + " kg");
	}
	
	//Setters and Getters
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getDepth() {
		return depth;
	}
	public void setDepth(double depth) {
		this.depth = depth;
	}
	public double volume() {
		return (this.height * this.width * this.depth);
	}
}

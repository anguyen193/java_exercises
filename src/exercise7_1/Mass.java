package exercise7_1;

public class Mass extends Prism {
	public String material;
	public double density;
	
	public Mass(double height, double width, double depth, String material, double density) {
		super(height, width, depth);
		this.material = material;
		this.density = density;
	}

	@Override
	public double calculate_volume() {
		return this.height * this.depth * this.width;
	}

	@Override
	public double calculate_mass() {		
		return this.calculate_volume() * this.density;
	}
}

package exercise4_1;

import java.util.Arrays;

public class Date {
	private int day;
	private int month;
	private int year;

	public static final String[] MONTHNAMES = {"January", "February",
		"March", "April", "May", "June", "July", "August",
		"September", "October", "November", "December"};

	public Date() {
		java.time.LocalDate today = java.time.LocalDate.now();
		this.day = today.getDayOfMonth();
		this.month = today.getMonthValue();
		this.year = today.getYear();
	}
	
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public Date(String date) {
		String[] date_parts = date.split("\\.");
		this.day = Integer.parseInt(date_parts[0]);
		this.month = Integer.parseInt(date_parts[1]);
		this.year = Integer.parseInt(date_parts[2]);
	}

	
	
	public String long_form_date() {
		if (this.day == 1 || this.day == 21 || this.day == 31) {
			return "The " + this.day + "st of " + MONTHNAMES[this.month - 1] + " " + this.year;
		} else if (this.day == 2 || this.day == 22) {
			return "The " + this.day + "nd of " + MONTHNAMES[this.month - 1] + " " + this.year;
		} else if (this.day == 3 || this.day == 23) {
			return "The " + this.day + "rd of " + MONTHNAMES[this.month - 1] + " " + this.year;
		} else {
			return "The " + this.day + "th of " + MONTHNAMES[this.month - 1] + " " + this.year;
		}
	}
	
	public static Integer convert_month_to_number(String month_name) {
		month_name = month_name.substring(0,1).toUpperCase() + month_name.substring(1).toLowerCase();
		return Arrays.asList(MONTHNAMES).indexOf(month_name) + 1;
	}

	public String toString() {
		return this.day + "." + this.month + "." + this.year;
	}
	
	public static void main(String args[]) {
		Date date_1 = new Date();
		Date date_2 = new Date("01.03.1996");

		System.out.println(date_1.toString());
		System.out.println(date_1.long_form_date());
		System.out.println(date_2.toString());
		System.out.println(date_2.long_form_date());
		
		System.out.println(Date.convert_month_to_number("januArY"));
	}

	public double getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public double getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public double getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
}

package exercise_3;

public class Circle {
	private double cx;
	private double cy;
	private double r;
	
	//Constructor
	public Circle(double cx, double cy, double r) {
		this.cx = cx;
		this.cy = cy;
		this.r = r;
	}
	
	public static void main(String[] args) {
		Circle obj = new Circle(4, 5, 7);
		System.out.println("x = " + obj.getCx() + " y = " + obj.getCy() + " r = " + obj.getR());
		
	}

	public double getCx() {
		return cx;
	}

	public void setCx(double cx) {
		this.cx = cx;
	}

	public double getCy() {
		return cy;
	}

	public void setCy(double cy) {
		this.cy = cy;
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}
	
	
}

package exercises1;

import java.util.Scanner;

public class Example1 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Your size?");
		int size = input.nextInt();
		int[] values = new int[size];
		
		for (int i = 0; i < size; i++) {
			values[i] = input.nextInt();
			System.out.println("value" + i + ": " + values[i]);
		}
		
		input.close();
	}
}

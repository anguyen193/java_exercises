package exam2;

import java.util.ArrayList;

public abstract class GymCard {
	private String customerName;
	private double price;
	public static ArrayList<GymCard> cards = new ArrayList<>();
	
	public GymCard(String name, double price) {
		this.customerName = name;
		this.price = price;
	}
	
	public StringBuilder to_string() {
		StringBuilder obj = new StringBuilder();
		obj.append(this.customerName + ", " + this.price);
		return obj;
	}
	
	public abstract boolean isValid();
}

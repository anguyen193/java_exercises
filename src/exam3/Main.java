package exam2;

public class Main {
	public static void main(String[] agrs) {
		MonthlyCard card1 = new MonthlyCard("Anh Nguyen", 100, "2018-03-19");
		MonthlyCard card2 = new MonthlyCard("Liam", 150, "2019-04-24");
		VisitCard card3 = new VisitCard("Petri Helo", 180, 1);
		VisitCard card4 = new VisitCard("Ville Salom�ki", 120, 24);
		
		GymCard.cards.add(card1);
		GymCard.cards.add(card2);
		GymCard.cards.add(card3);
		GymCard.cards.add(card4);
		
		card3.decreaseVisit();
		
		for (GymCard card : GymCard.cards) {
			System.out.print(card.to_string());
			if (card.isValid()) {
				System.out.println("The card is valid");
			} else {
				System.out.println("The card is not valid");
			}
		}
		
	}
}

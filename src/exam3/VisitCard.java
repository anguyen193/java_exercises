package exam2;

public class VisitCard extends GymCard {
	private int remainingVisits;
	
	public VisitCard(String name, double price, int remainingVisits) {
		super(name, price);
		this.remainingVisits = remainingVisits;
	}

	public StringBuilder to_string() {
		StringBuilder obj = new StringBuilder();
		obj.append(super.to_string());
		obj.append(", remaining visits " + this.remainingVisits + ". ");
		
		return obj;
	}
	
	public boolean isValid() {
		if (this.remainingVisits > 0) {
			return true;
		}
		return false;
	}
	
	public int decreaseVisit() {
		return this.remainingVisits--;
	}
}

package exam2;

public class MonthlyCard extends GymCard {
	private String expirationDate; 

	public MonthlyCard(String name, double price, String expirationDate) {
		super(name, price);
		this.expirationDate = expirationDate;
	}

	public StringBuilder to_string() {
		StringBuilder obj = new StringBuilder();
		obj.append(super.to_string());
		obj.append(", expiration date " + this.expirationDate + ". ");
		
		return obj;
	}
	
	public boolean isValid() {
		String[] dateParts = this.expirationDate.split("-");
		int day = Integer.parseInt(dateParts[2]);
		int month = Integer.parseInt(dateParts[1]);
		int year = Integer.parseInt(dateParts[0]);
		
		java.time.LocalDate today = java.time.LocalDate.now();
		int todayDay = today.getDayOfMonth();
		int todayMonth = today.getMonthValue();
		int todayYear = today.getYear();
		
		if (year > todayYear) {
			return true;
		} else if (year < todayYear) {
			return false;
		} else {
			if (month > todayMonth) {
				return true;
			} else if (month < todayMonth) {
				return false;
			} else {
				if (day >= todayDay) {
					return true;
				} else {
					return false;
				}
			}
		}
	}
}

package exercise8_3;

import javax.swing.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

public class DistributionBars extends JPanel {
	private static final long serialVersionUID = 1L;
	private int min;
	private int max;
	private int count;
	private int[] freq;

	//Constructor
	public DistributionBars() {
		setBackground(Color.white);
		this.min = 0;
		this.max = 5;
		this.count = 0;
		freq = new int[max - min + 1];
	}
	
	public void insertValue(int value) {
		if (value >= min && value <= max) {
			freq[value - min]++;
		}
		repaint();
	}
	
	public int getFrequency(int value) {
		if (value >= min && value <= max) {
			return freq[value - min];
		} else {
			return 0;
		}
	}

	public double getAverage() {
		int sum = 0;
		for (int i = 0; i < freq.length; i++) {
			sum = sum + (this.min + i) * freq[i];
		}
		double average = (double)sum / this.count;
		return average;
	}

	public void count(int value) {
		if (value >= min && value <= max) {
			this.count++;
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int barWidth = 0;
		double scale = 0;

		if (this.freq == null || this.freq.length == 0)
			return;
		int minValue = 0;
		int maxValue = 0;
		for (int i = 0; i < this.freq.length; i++) {
			if (minValue > freq[i])
				minValue = freq[i];
			if (maxValue < freq[i])
				maxValue = freq[i];
		}
//		System.out.println(maxValue);
//		System.out.println(minValue);
		Dimension d = getSize();
//
		int clientWidth = d.width;
		int clientHeight = d.height;
		if ((this.max - this.min) != -1) {
			barWidth = clientWidth / (this.max - this.min + 1);
		}
		Font titleFont = new Font("SansSerif", Font.BOLD, 20);
		FontMetrics titleFontMetrics = g.getFontMetrics(titleFont);
		Font labelFont = new Font("SansSerif", Font.PLAIN, 10);
		FontMetrics labelFontMetrics = g.getFontMetrics(labelFont);
		int top = titleFontMetrics.getHeight();
		int bottom = labelFontMetrics.getHeight();
		if ((maxValue - minValue) != 0) {
			scale = (clientHeight - top - bottom) / (maxValue - minValue);
		}

		for (int i = this.min; i <= this.max; i++) {
			int valueX = i * barWidth + 1;
			int valueY = top;
			int height = (int) (this.getFrequency(i) * scale);
			if (getFrequency(i) >= 0)
				valueY += (int) ((maxValue - i * scale));
			else {
				valueY += (int) (maxValue * scale);
				height = -height;
			}
			g.setColor(Color.red);
			g.fillRect(valueX, valueY, barWidth - 2, freq[i]*10);
			g.setColor(Color.black);
			g.drawRect(valueX, valueY, barWidth - 2, freq[i]*10);
		}
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int[] getFreq() {
		return freq;
	}

	public void setFreq(int[] freq) {
		this.freq = freq;
	}
}

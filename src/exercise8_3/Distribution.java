package exercise8_3;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
//import java.awt.Color;
import java.awt.BorderLayout;

public class Distribution extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private DistributionBars bars = new DistributionBars();
	private JTextField textField = new JTextField(5);

	public Distribution() {
		this.setTitle("Distribution");
		
		JPanel inputBar = new JPanel();
		
		//Add input
        JLabel label = new JLabel("Grade: ");
        inputBar.add(label);

        inputBar.add(textField);

        JButton insertBtn = new JButton("Insert");
        insertBtn.addActionListener(this);
        inputBar.add(insertBtn);

        //Position input bar
      	this.add(inputBar, BorderLayout.NORTH);
      	this.add(bars, BorderLayout.CENTER);

      	setDefaultCloseOperation(EXIT_ON_CLOSE);
      	setSize(600, 500);
      	setLocationRelativeTo(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int insertedValue = Integer.parseInt(textField.getText());
		bars.insertValue(insertedValue);
	}
}

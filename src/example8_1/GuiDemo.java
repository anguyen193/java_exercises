package example8_1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class GuiDemo extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	//GUI components
    private JTextField txtValue1 = new JTextField(10);
    private JTextField txtValue2 = new JTextField(10);
    private JButton btnCalculate = new JButton("Calculate");
    private JLabel lblResult= new JLabel("Result is ");

    //Constructor
    public GuiDemo() {
        this.setTitle("GUI Demo");
        this.setLayout(null); 

        //add components to the frame
        JLabel t1 = new JLabel("Value 1");
        t1.setBounds(30, 20, 50, 20);
        this.add(t1);
        txtValue1.setBounds(85, 20, 70, 20);
        this.add(txtValue1);

        JLabel t2 = new JLabel("Value 2");
        t2.setBounds(30, 50, 50, 20);
        this.add(t2);
        txtValue2.setBounds(85, 50, 70, 20);
        this.add(txtValue2);

        btnCalculate.setBounds(30, 80, 150, 25);
        this.add(btnCalculate);
        lblResult.setBounds(30, 120, 300, 20);
        this.add(lblResult);

        //set size of the frame
        this.setSize(400, 200);

        //closing the window will close the application
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
        //register listener
        btnCalculate.addActionListener(this);
    } //end of constructor

    //What happens when the user clicks the button
    @Override
    public void actionPerformed(ActionEvent evt) {
        double val1, val2;
        val1 = Double.parseDouble(txtValue1.getText());
        val2 = Double.parseDouble(txtValue2.getText());
        double sum = val1 + val2;
        lblResult.setText("Result is " + sum);
    } //end of actionPerformed

    public static void main(String[] args) {
        GuiDemo frame = new GuiDemo();
        frame.setVisible(true);
    } //end of main
}

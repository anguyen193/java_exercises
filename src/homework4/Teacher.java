package homework4;

/*
 * Complete this example. In the Teacher class, implement a constructor that gets the name of the teacher as
 * a parameter. Implement the getters for the name of the teacher and the name of the course. Write
 * toString methods to both classes. The toString method of the Teacher class returns the name of the teacher
 * and the names of his/hers courses as a string. The toString method of the Course class returns the name of
 * the course and the name of its teacher as a string.
 * Write a short test program that creates some Teacher objects and some Course objects and prints out
 * those using toString method.
 */

import java.util.ArrayList;

public class Teacher {
	private String nameOfTeacher;
	private ArrayList<Course> courses = new ArrayList<>();
	
	public Teacher(String name) {
		this.nameOfTeacher = name;
	}
	
	public StringBuilder to_string() {
		StringBuilder obj = new StringBuilder();
		obj.append("Teacher: " + this.nameOfTeacher + "\n");
		obj.append("Courses:\n");
		for (Course course : this.courses) {
			obj.append("    " + course.getNameOfCourse() + "\n");
		}
		return obj;
	}
	
	public static void main(String agrs[]) {
		Teacher teacher1 = new Teacher("Juka Matila");
		Course course1 = new Course("Basics of Linux", teacher1);
		Course course2 = new Course("C programming", teacher1);
		
		Teacher teacher2 = new Teacher("Ville Salom�ki");
		Course course3 = new Course("OOP", teacher2);
		Course course4 = new Course(".NET", teacher2);
		
		System.out.print(teacher1.to_string());
		System.out.print(course1.to_string());
	}
	
	public void insertCourse(Course course) {
		courses.add(course);
	}

	public String getNameOfTeacher() {
		return nameOfTeacher;
	}

	public void setNameOfTeacher(String nameOfTeacher) {
		this.nameOfTeacher = nameOfTeacher;
	}

	public ArrayList<Course> getCourses() {
		return courses;
	}

	public void setCourses(ArrayList<Course> courses) {
		this.courses = courses;
	}
	
	
}

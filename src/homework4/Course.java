package homework4;

public class Course {
	private Teacher teacher;
	private String nameOfCourse;
	
	public Course(String nameOfCourse, Teacher teacher){
		this.nameOfCourse = nameOfCourse;
		this.teacher = teacher;
		
		if (teacher != null){
			teacher.insertCourse(this);
		}
	}
	
	public StringBuilder to_string() {
		StringBuilder obj = new StringBuilder();
		obj.append("Course: " + this.nameOfCourse + " instructed by " + this.teacher.getNameOfTeacher());
		
		return obj;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public String getNameOfCourse() {
		return nameOfCourse;
	}

	public void setNameOfCourse(String nameOfCourse) {
		this.nameOfCourse = nameOfCourse;
	}
}

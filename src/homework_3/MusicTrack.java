package homework_3;

import java.util.ArrayList;
import java.util.Scanner;

public class MusicTrack {
	private String title;
	private String artist;
	private int duration;
	private static ArrayList<MusicTrack> play_list = new ArrayList<>();
	
	public MusicTrack(String title, String artist, int duration) {
		this.title = title;
		this.artist = artist;
		this.duration = duration;
	}
	
	static void add_track() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Title: ");
		String title = sc.nextLine();

		System.out.print("Artist: ");
		String artist = sc.nextLine();
		
		System.out.print("Duration: ");
		int duration = sc.nextInt();
		
		System.out.print("Position to add: ");
		int position = sc.nextInt();
		
		MusicTrack new_track = new MusicTrack(title, artist, duration);
		
		if (position-1 > play_list.size()) {
			do {
				System.out.print("Error. The position you chose has exeeded the size of the play list (" + play_list.size() +"). \nPlease choose another position: ");
				position = sc.nextInt();
			} while (position-1 > play_list.size()); 
		}
		play_list.add(position-1, new_track);

	}
	
	static void move_track() {
		Scanner sc = new Scanner(System.in);

		System.out.print("Track to move: ");
		String track_to_move = sc.nextLine();
		
		System.out.print("New position: ");
		int new_position = sc.nextInt();

		MusicTrack track = find_track(track_to_move);

		if (track != null) {
			play_list.remove(track);
			play_list.add(new_position, track);
		} else {
			System.out.print("Error. No track named "+ track_to_move +" found. \n");
		}
	}
	
	static MusicTrack find_track(String track_to_find) {
		for (MusicTrack track : play_list) {
			if (track.getTitle().equals(track_to_find));
			return track;
		}
		return null;
	}
	
	public static void main(String agrs[]) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.println("Do you want to insert or move a track?");
			String user_answer = sc.next(); 

			if (user_answer.toLowerCase().equals("insert")) {
				add_track();
			} else if (user_answer.toLowerCase().equals("move")) {
				move_track();
			}			
			System.out.println("Play list UPDATED");
			for (MusicTrack track : play_list) {
				System.out.print("Title: " + track.getTitle() + "     ");
				System.out.print("Artist: " + track.getArtist() + "     ");
				System.out.print("Duration: " + track.getDuration());
				System.out.print("\n");
			}
		} while (1 > 0);
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
}

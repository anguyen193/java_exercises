package exercise2_1;

import java.util.Scanner;

public class Volume {
	public void process() {
		Scanner sc = new Scanner(System.in);
		Prism prism = new Prism();

		System.out.print("Choose height for the Prism:");
		double height = sc.nextDouble();
		prism.setHeight(height);
		
		System.out.print("Choose width for the Prism:");
		double width = sc.nextDouble();
		prism.setWidth(width);
		
		System.out.print("Choose depth for the Prism:");
		double depth = sc.nextDouble();
		prism.setDepth(depth);
		
		double volume = prism.volume();
		System.out.println("The volume of the Prism is: " + volume);
		
		sc.close();
	}
	
	public static void main(String args[]) {
		Volume program = new Volume();
		program.process();
	}
}

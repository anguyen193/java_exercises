package exercise7_3;

import java.util.Scanner;

public class ClassRoom extends Class{
	public ClassRoom() {
		super();
	}

	private int numberOfSeats;
	private int numberOfComputers;
	private boolean isProjector;
	
	public StringBuilder to_string() {
		StringBuilder obj = new StringBuilder();
		obj.append(super.to_string());
		obj.append("    Number of seats: " + this.numberOfSeats
				+ ", number of computers: " + this.numberOfComputers + ", data projector: ");
		if (this.isProjector == true) {
			obj.append("available\n");
		} else {
			obj.append("not available\n");
		}
		return obj;
	}
	
	public void insertRoom() {
		Scanner sc = new Scanner(System.in);

		super.insertRoom();
		
		System.out.print("Insert number of seats in the room: ");
		int seats = sc.nextInt();
		
		System.out.print("Insert number of computers in the room: ");
		int computers = sc.nextInt();
		
		System.out.print("Does the room have data projector(s)?");
		String projector = sc.next();
		
		this.numberOfSeats = seats;
		this.numberOfComputers = computers;
		
		if (projector.toLowerCase().equals("y") || projector.toLowerCase().equals("true") || projector.toLowerCase().equals("yes")) {
			this.isProjector = true;
		} else {
			this.isProjector = false;
		}
	}
}

package exercise7_3;

import java.util.ArrayList;
import java.util.Scanner;

public class Class {
	private String roomNumber;
	private String roomName;
	public static ArrayList<Class> roomList = new ArrayList<>();  
	
	public Class() {
		
	}

	public StringBuilder to_string() {
		StringBuilder obj = new StringBuilder();
		obj.append(this.roomNumber + ": " + this.roomName + "\n");
		return obj;
	}
	
	public void insertRoom() {
		Scanner sc = new Scanner(System.in);

		System.out.print("Insert room number: ");
		String roomNumber = sc.nextLine();

		System.out.print("Insert room name: ");
		String roomName = sc.nextLine();
		
		Class checkingRoom = searchRoom(roomNumber);
		if (checkingRoom != null) {
			do {
				System.out.print("Room number already existed. Please choose another room number: ");
				roomNumber = sc.nextLine();
				checkingRoom = searchRoom(roomNumber);
			} while (checkingRoom != null);
		}
		
		this.roomNumber = roomNumber;
		this.roomName = roomName;
	}
	
	public static Class searchRoom(String roomToSearch) {
		for (Class room : roomList) {
			if (room.roomNumber.equals(roomToSearch)) {
				return room;
			}
		}
		return null;
	}

	public static void main(String agrs[]) {
		Scanner sc = new Scanner(System.in);
		int choice;
		do {
			System.out.println(""
				+ "Enter your choice:\n "
				+ "1. Insert a room\n "
				+ "2. Print out all rooms\n "
				+ "3. Print out offices\n "
				+ "4. Print out class rooms\n "
				+ "5. Print out other rooms (not office, not class room)\n "
				+ "6. Search a room\n "
				+ "7. Search a staff member"
			);
			choice = sc.nextInt();
			
			switch(choice) {
				case 1:
					System.out.print("Insert a Room, Office or Classroom? ");
					String roomChoice = sc.next().toLowerCase();
					Class obj;
					switch (roomChoice) {
						case "room":
							obj = new Class();
							obj .insertRoom();
							roomList.add(obj);
						  	break;
						case "office":
							obj = new Office();
							obj.insertRoom();
							roomList.add(obj);
							break;
						case "classroom":
							obj = new ClassRoom();
							obj.insertRoom();
							roomList.add(obj);
							break;
					}
					break;
				case 2:
					for (Class room : roomList) {
						System.out.print(room.to_string());
					}
					break;
				case 3:
					for (Class office : roomList) {
						if (office instanceof Office) {
							System.out.print(office.to_string());
						}
					}
					break;
				case 4:
					for (Class classroom : roomList) {
						if (classroom instanceof ClassRoom) {
							System.out.print(classroom.to_string());
						}
					}
					break;
				case 5:
					for (Class other : roomList) {
						if (!(other instanceof ClassRoom) && !(other instanceof Office)) {
							System.out.print(other.to_string());
						}
					}
					break;
				case 6:
					sc.nextLine();

					System.out.print("Room number to search: ");
					String roomToSearch = sc.nextLine();
					Class result = searchRoom(roomToSearch);
					
					if (result != null) {
						System.out.print(result.to_string());
					} else {
						System.out.print("Error. No rooms named " + roomToSearch + " found.\n");
					}
					break;
				case 7:
					sc.nextLine();
					System.out.print("Room number to search: ");
					roomToSearch = sc.nextLine();
					Office resultRoom = (Office) searchRoom(roomToSearch);
					if (resultRoom != null) {
						System.out.print("Staff member to search: ");
						String staffToSearch = sc.nextLine();
						if (resultRoom instanceof Office) {
							if (resultRoom.worksHere(staffToSearch) == true) {
								System.out.print(resultRoom.to_string());
							} else {
								System.out.print("Error. No staff members named " + staffToSearch + " found in room " + resultRoom.getroomNumber() + ".\n");
							}
						} else {
							System.out.print("Error. Room " + resultRoom.getroomNumber() + " is not an Office.\n");
						}
					} else {
						System.out.print("Error. No rooms named " + roomToSearch + " found.\n");
					}
					break;
			}
		} while (choice != 0);
	}

	public String getroomNumber() {
		return roomNumber;
	}

	public void setroomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getroomName() {
		return roomName;
	}

	public void setroomName(String roomName) {
		this.roomName = roomName;
	}
}

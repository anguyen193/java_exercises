package exercise7_3;

import java.util.ArrayList;
import java.util.Scanner;

public class Office extends Class {
	public ArrayList<String> staffMembers = new ArrayList<>();
	
	public Office() {
		super();
	}

	public StringBuilder to_string() {
		StringBuilder obj = new StringBuilder();
		obj.append(super.to_string());
		obj.append("    Staff members: ");
		for (String member : this.staffMembers) {
			obj.append(member + "   ");
		}
		obj.append("\n");
		return obj;
	}
	
	public void insertRoom() {
		Scanner sc = new Scanner(System.in);

		super.insertRoom();
		String name = "somename";
		do {
			System.out.print("Insert staff member's name: ");
			name = sc.nextLine();
			this.staffMembers.add(name);
		} while (!name.equals(""));
	}

	public boolean worksHere(String staffMember) {
		return this.staffMembers.contains(staffMember) ? true : false;
	}
}

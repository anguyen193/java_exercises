package project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Temperature {
	@SuppressWarnings("rawtypes")
	public ArrayList<HashMap> weatherCollection = new ArrayList<>();
	private String year, month, day;
	private String existingFile;
	
	public Temperature(String day, String month, String year) throws IOException {
		month = Integer.toString(this.getMonthNumber(month));
		this.year = year;
		this.month =  month;
		this.day = day;
		String time;
		String temp;
		String wind;
		String fullDate;
		HashMap<String, String> row = new HashMap<>();
		
		if (this.fileExists()) {
			System.out.println("Weather info from text file");
			try(BufferedReader input = new BufferedReader(new FileReader(existingFile))) {
		        String line;
		        String[] lineParts;
				
		        while((line = input.readLine()) != null ) {
		        	lineParts = line.split(",");
		        	fullDate = lineParts[0];
		        	wind = lineParts[1];
		        	temp = lineParts[2];
		        	time = lineParts[3];
		        	
		        	//Append weather information into the created Hashmap and ArrayList
					row.put("time", time);
					row.put("temperature", temp);
					row.put("wind speed", wind);
					row.put("dateUTC", fullDate);
					this.weatherCollection.add(row);
					
					//Reset the variables
					row = new HashMap<String, String>();
					time = "";
					temp = "";
					wind = "";
		        }
		    }
		    catch (FileNotFoundException ex1) {
		    	System.out.println(ex1);
		    }
		    catch(IOException ex2){
		       //TODO
		    }
		} else {
			System.out.println("Weather info from URL");
			String address = "https://www.wunderground.com/history/airport/EFVA/" + year + "/" + month + "/" + day + "/"
					+ "DailyHistory.html?req_city=Vaasa+Airport&req_state=&req_statename="
					+ "Finland&reqdb.zip=00000&reqdb.magic=&reqdb.wmo=&format=1";
			
			try (InputStream in = new URL(address).openStream();
			BufferedReader rin = new BufferedReader(new InputStreamReader(in))) {
				String line;
				String[] weatherInfo;
				
				//Skip the first 2 lines
				rin.readLine();
				rin.readLine();
				
				while((line = rin.readLine()) != null) {
					weatherInfo = line.split(",");
					time = weatherInfo[0];
					temp = weatherInfo[1];
					wind = weatherInfo[7];
					fullDate = weatherInfo[13];
					
					//Append weather information into the created Hashmap and ArrayList
					row.put("time", time);
					row.put("temperature", temp);
					row.put("wind speed", wind);
					row.put("dateUTC", fullDate);
					this.weatherCollection.add(row);
					
					//Reset the variables
					row = new HashMap<String, String>();
					time = "";
					temp = "";
					wind = "";
				}
			}
			catch(MalformedURLException ex1) {
				System.out.println(ex1);
			}
			catch(IOException ex2) {
				System.out.println(ex2);
			}
		}
	}
	
	public String getAverageTemp() {
		double sum = 0;
		int size = weatherCollection.size();
		for (HashMap singleTime: this.weatherCollection) {
			sum += Double.parseDouble((String) singleTime.get("temperature"));
		}
		return String.format(Locale.ENGLISH, "%.2f", (Double) (sum / size));
	}
	
	public String getAverageWind() {
		double sum = 0;
		int size = weatherCollection.size();
		for (HashMap singleTime: this.weatherCollection) {
			double speed;
			if ((boolean) ((String) singleTime.get("wind speed")).matches("[-+]?\\d*\\.?\\d+")) {
				speed = Double.parseDouble((String) singleTime.get("wind speed"));
			} else {
				speed = 1;
			}
			sum += speed;
		}
		return String.format(Locale.ENGLISH, "%.2f", (Double) (sum / size));
	}
	
	public double getMaxTemp() {
		double currentTemp;
		double maxTemp = -100;
		for (HashMap singleTime: this.weatherCollection) {
			currentTemp = Double.parseDouble((String) singleTime.get("temperature"));
			if (currentTemp > maxTemp) {
				maxTemp = currentTemp;
			}
		}
		return maxTemp;
	}
	
	public double getMinTemp() {
		double currentTemp;
		double minTemp = 100;
		for (HashMap singleTime: this.weatherCollection) {
			currentTemp = Double.parseDouble((String) singleTime.get("temperature"));
			if (currentTemp < minTemp) {
				minTemp = currentTemp;
			}
		}
		return minTemp;
	}
	
	// Add the leading zero if the day and month are less than 10
	public List<String> formatDate() {
		String formattedMonth, fomattedDay;
		
		if (Integer.parseInt(this.month) < 10) {
			formattedMonth = String.format("%02d", Integer.parseInt(this.month));
		} else {
			formattedMonth = this.month;
		}
		
		if (Integer.parseInt(this.day) < 10) {
			fomattedDay = String.format("%02d", Integer.parseInt(this.day));
		} else {
			fomattedDay = this.day;
		}
		
		return Arrays.asList(formattedMonth, fomattedDay);
	}
	
	// Check if being-searched date has been already stored as a text file
	public boolean fileExists() {
		String fileName = "weatherCollection/" + this.year + this.formatDate().get(0) + this.formatDate().get(1) + ".txt";
		File file = new File(fileName);
		
		if (file.exists()) {
			this.existingFile = fileName;
			return true;
		}
		return false;
	}
	
	// Save weather into a text file
	@SuppressWarnings("rawtypes")
	public void saveWeather() throws FileNotFoundException {
		try (FileWriter file = new FileWriter("weatherCollection/" + this.year + this.formatDate().get(0) + this.formatDate().get(1) + ".txt")) {
			String singleLine = " ";
			
			for (HashMap singleTime: this.weatherCollection) {
				singleLine = singleTime.get("dateUTC") + "," + singleTime.get("wind speed") + "," + singleTime.get("temperature") + "," + singleTime.get("time") + "\r\n";
				file.write(singleLine);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getMonthNumber(String monthName) {
	    return Month.valueOf(monthName.toUpperCase()).getValue();
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}
}

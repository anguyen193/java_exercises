package project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TemperatureLayout extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JComboBox<String> dayComboBox;
	private JComboBox<String> monthComboBox;
	private JComboBox<String> yearComboBox;
	private TemperatureGraph graph = new TemperatureGraph();

	public TemperatureLayout() {
		//Set title
		this.setTitle("Temperature Graph");
		
		/*
		 * Input bar
		 */
		JPanel inputBar = new JPanel();
//		inputBar.setBackground(Color.WHITE);
		
		JLabel dayLabel = new JLabel("Day");
		inputBar.add(dayLabel);
		
		String[] dayArr = new String[31];
		for (int i = 0; i < dayArr.length; i++) {  
			dayArr[i] = Integer.toString(i+1);
		}
		dayComboBox = new JComboBox<>(dayArr);
		inputBar.add(dayComboBox);
		
		JLabel monthLabel = new JLabel("Month");
		inputBar.add(monthLabel);
		
		String[] monthArr = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		monthComboBox = new JComboBox<>(monthArr);
		inputBar.add(monthComboBox);
		
		JLabel yearLabel = new JLabel("Year");
		inputBar.add(yearLabel);
		
		String[] yearArr = new String[8];
		for (int k = 0; k < yearArr.length; k++) {  
			yearArr[k] = Integer.toString(k + 2010);
		}
		yearComboBox = new JComboBox<>(yearArr);
		yearComboBox.setSelectedItem(yearArr[7]);
		inputBar.add(yearComboBox);
		
		JButton insertBtn = new JButton("Search Weather");
        insertBtn.addActionListener(this);
        insertBtn.setLocation(50, 30);
        inputBar.add(insertBtn);
		
      	this.add(inputBar, BorderLayout.NORTH);
      	/*End of input bar*/
      	
      	/*
      	 * Graph bar
      	 */
      	this.add(graph, BorderLayout.CENTER);
      	
      	setDefaultCloseOperation(EXIT_ON_CLOSE);
      	setSize(1500, 800);
      	setLocationRelativeTo(null);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String day, month, year;
		day = (String) dayComboBox.getSelectedItem();
		month = (String) monthComboBox.getSelectedItem();
		year = (String) yearComboBox.getSelectedItem();
		
		Temperature obj;
		try {
			obj = new Temperature(day, month, year);
			obj.saveWeather();
			graph.setData(obj.weatherCollection);
			graph.setMaxTemp(obj.getMaxTemp());
			graph.setMinTemp(obj.getMinTemp());
			graph.setAvgTemp(obj.getAverageTemp());
			graph.setAvgWind(obj.getAverageWind());
			graph.setDate(day, month, year);
			graph.repaint();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}
}

package project;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JPanel;

public class TemperatureGraph extends JPanel {
	private static final long serialVersionUID = 1L;
	private static int BORDER_GAP = 50;
	private static int GRAPH_POINT_WIDTH = 8;
	private static Color GRAPH_COLOR = Color.blue;
	private static Stroke GRAPH_STROKE = new BasicStroke(3f);
	private static Color GRAPH_POINT_COLOR = Color.blue;
	@SuppressWarnings("rawtypes")
	private ArrayList<HashMap> data = new ArrayList<>();
	private double maxTemp = 10;
	private double minTemp = -10;
	private double avgTemp = 0;
	private double avgWind = 0;
	private String date = "";

	public TemperatureGraph() {
		setBackground(Color.white);
	}

	public Set<Double> getUniqueTempList(ArrayList<HashMap> data) {
		ArrayList<Double> duplicateTempList = new ArrayList<>();
		 
		for (HashMap singleValue : data) {
			duplicateTempList.add(Double.parseDouble((String) singleValue.get("temperature")));
		}
		Set<Double> uniqueTempList = new HashSet<>(duplicateTempList);
		return uniqueTempList;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D)g;
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		double xScale = ((double) getWidth() - 2 * BORDER_GAP) / (data.size() - 1);
	    double yScale = ((double) getHeight() - 2 * BORDER_GAP) / ((this.maxTemp - this.minTemp));
	    
	    //Generate points' coordinates
	    ArrayList<Point> graphPoints = new ArrayList<Point>();
	    for (int i = 0; i < this.data.size(); i++) {
	         int x1 = (int) (i * xScale + BORDER_GAP);
	         int y1 = (int) ((this.maxTemp - Double.parseDouble((String) this.data.get(i).get("temperature"))) * yScale + BORDER_GAP);
	         graphPoints.add(new Point(x1, y1));
	    }
	    
	    // Draw Title
	    Font myFont = new Font("Calibri", 0, 20);
        g2.setFont(myFont);
        g2.drawString("Weather " + this.date, getWidth()/2 - 100, 20);
	    g2.drawString("Average temperature " + this.avgTemp + "�C, wind speed " + this.avgWind + "km/h", getWidth()/2 - 180, 40);
	    
	    // Create x and y axes 
	    g2.drawLine(BORDER_GAP, getHeight() - BORDER_GAP, BORDER_GAP, BORDER_GAP);
	    g2.drawLine(BORDER_GAP, getHeight() - BORDER_GAP, getWidth() - BORDER_GAP, getHeight() - BORDER_GAP);
	    
	    // Create tick marks for y axis.
	    for (int i = 0; i < this.getUniqueTempList(this.data).size(); i++) {
	        int x0 = BORDER_GAP;
	        int x1 = GRAPH_POINT_WIDTH + BORDER_GAP;
	        int y0 = getHeight() - (((i + 1) * (getHeight() - BORDER_GAP * 2)) / (this.getUniqueTempList(this.data).size()-1) + BORDER_GAP);
	        int y1 = y0;
	        g2.drawLine(x0, y0, x1, y1);
	    }
	    
	    // Create tick marks for x axis
	    for (int i = 0; i < this.data.size() - 1; i++) {
	        int x0 = (i + 1) * (getWidth() - BORDER_GAP * 2) / (this.data.size() - 1) + BORDER_GAP;
	        int x1 = x0;
	        int y0 = getHeight() - BORDER_GAP;
	        int y1 = y0 - GRAPH_POINT_WIDTH;
	        g2.drawLine(x0, y0, x1, y1);
	    }
	    
	    // Create tick labels for y axis
	    for (int i = 0; i < this.getUniqueTempList(this.data).size(); i++) {
	        int x = BORDER_GAP - 30;
	        int y = getHeight() - (((i) * (getHeight() - BORDER_GAP * 2)) / (this.getUniqueTempList(this.data).size()-1) + BORDER_GAP);
	        Font yLabel = new Font("Calibri", 0, 15);
	        g2.setFont(yLabel);
	        g2.drawString(Integer.toString((int) (this.minTemp + i)), x, y);
	    }
	    
	    // Create tick labels for x axis
	    for (int i = 0; i < this.data.size() - 1; i+=4) {
	        int x = (i) * (getWidth() - BORDER_GAP * 2) / (this.data.size() - 1) + BORDER_GAP - 20;
	        int y = getHeight() - BORDER_GAP + 20;
	        Font xLabel = new Font("Calibri", 0, 15);
	        g2.setFont(xLabel);
	        g2.drawString((String) this.data.get(i).get("time"), x, y);
	    }
	    
	    // Draw the temperature graph
	    Stroke oldStroke = g2.getStroke();
	    g2.setColor(GRAPH_COLOR);
	    g2.setStroke(GRAPH_STROKE);
	    for (int i = 0; i < graphPoints.size() - 1; i++) {
	        int x1 = graphPoints.get(i).x;
	        int y1 = graphPoints.get(i).y;
	        int x2 = graphPoints.get(i + 1).x;
	        int y2 = graphPoints.get(i + 1).y;
	        g2.drawLine(x1, y1, x2, y2);         
	    }
	    
	    // Highlight up the temperature points
	    g2.setStroke(oldStroke);      
	      g2.setColor(GRAPH_POINT_COLOR);
	      for (int i = 0; i < graphPoints.size(); i++) {
	         int x = graphPoints.get(i).x - GRAPH_POINT_WIDTH / 2;
	         int y = graphPoints.get(i).y - GRAPH_POINT_WIDTH / 2;
	         int ovalW = GRAPH_POINT_WIDTH;
	         int ovalH = GRAPH_POINT_WIDTH;
	         g2.fillOval(x, y, ovalW, ovalH);
	      }
	}
	public String getSeperator(int day) {
		if (day == 1 || day == 21 || day == 31) {
			return "st";
		} else if (day == 2 || day == 22) {
			return "nd";
		} else if (day == 3 || day == 23) {
			return "rd";
		} else {
			return "th";
		}
	}

	public ArrayList<HashMap> getData() {
		return data;
	}

	public void setData(ArrayList<HashMap> data) {
		this.data = data;
	}
	
	public double getMaxTemp() {
		return maxTemp;
	}

	public void setMaxTemp(double maxTemp) {
		this.maxTemp = maxTemp;
	}

	public double getMinTemp() {
		return minTemp;
	}

	public void setMinTemp(double minTemp) {
		this.minTemp = minTemp;
	}

	public double getAvgTemp() {
		return avgTemp;
	}


	public void setAvgTemp(String avgTemp) {
		this.avgTemp = Double.parseDouble(avgTemp);
	}


	public double getAvgWind() {
		return avgWind;
	}


	public void setAvgWind(String avgWind) {
		this.avgWind = Double.parseDouble(avgWind);
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String day, String month, String year) {
		String seperator = this.getSeperator(Integer.parseInt(day));
		this.date = month + ", " + day + seperator + " " + year;
	}

}

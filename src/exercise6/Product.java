package exercise6;

import java.util.HashMap;
import java.util.Scanner;

public class Product {
	public static HashMap<Integer, Double> prod_info = new HashMap<Integer, Double>();

	public static void main(String agrs[]) {
		Scanner sc = new Scanner(System.in);
		
		Product.prod_info.put(1, 12.5);
		Product.prod_info.put(2, 22.5);
		Product.prod_info.put(3, 2.5);
		Product.prod_info.put(4, 52.5);
		Product.prod_info.put(5, 17.5);
		Product.prod_info.put(6, 4.5);
		Product.prod_info.put(7, 18.7);
		Product.prod_info.put(8, 23.2);
		
		do {
			System.out.print("Product number: ");
			int prod_no = sc.nextInt();

			if (Product.prod_info.containsKey(prod_no)) {
				System.out.print("Price of Product number " + prod_no + ": " + Product.prod_info.get(prod_no) + "\n");
			} else {
				System.out.print("No Product number " + prod_no + " found \n");
			}
		} while (1 > 0);
	}
	
	public HashMap<Integer, Double> getProd_info() {
		return prod_info;
	}

	public void setProd_info(HashMap<Integer, Double> prod_info) {
		this.prod_info = prod_info;
	}
}
